from selenium import webdriver
from time import sleep
import numpy as np
import pyautogui
import cv2
import tldextract

class WebPageRecorder:
    def __init__(self):
        self.urls = ['https://n26.com/fr-fr', 'https://robinhood.com/us/en/']
        self.webdriver_path = '/home/jamal/chromedriver'
        self.driver = webdriver.Chrome(self.webdriver_path)
        self.resolution = pyautogui.size()
        self.codec = cv2.VideoWriter_fourcc(*'XVID')
        self.fps = 10.0

    def get_video_as_file(self, filename):
        out = cv2.VideoWriter(filename, self.codec, self.fps, self.resolution)
        for i in range(300):
            try:
                img = pyautogui.screenshot()
                image = cv2.cvtColor(np.array(img), cv2.COLOR_RGB2BGR)
                out.write(image)
                StopIteration(0.5)
            except KeyboardInterrupt:
                break
        out.release()
        cv2.destroyAllWindows()

    def capture_web_page(self):
        for url in self.urls:
            ext = tldextract.extract(url)
            self.driver.set_window_size(1024, 768)
            self.driver.get(url)
            sleep(1)
            print('driver', self.driver)
            self.get_video_as_file(ext.domain+".avi")
            print("ended 30 seconds video for:", url)
        self.driver.quit()
        print("end...")

if __name__ == "__main__":
    run = WebPageRecorder()
    run.capture_web_page()